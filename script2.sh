#!/bin/bash

DOCKER_CMD=docker
#CONTAINERS=(jenkins sonatype/nexus3 sonarqube nginx alpine)
CONTAINERS=(alpine)
BRIDGE_NAME=net-bridge

NEW_IMAGES=()

#verify if images exists
echo "Checking images..."

for container in "${CONTAINERS[@]}" ; do
    if [[ "$(docker images -q $container 2> /dev/null)" == "" ]]; then
        echo "$container image not found"
        NEW_IMAGES+=($container)
    else 
        echo "$container found"
    fi    
done
#create bridge network
createNetwork(){
    printf "\n==== Creating >> $BRIDGE_NAME << network ===="
    $DOCKER_CMD network create $BRIDGE_NAME
    
}
createVolumes(){
    for image in "${NEW_IMAGES[@]}" ; do
        volume_name="${image//\/}"
        echo $new_name
        printf "\n ==== Creating Volume for $image ===="
        $DOCKER_CMD volume create $volume_name
    done
    
}
#function for pulling images
pullImages(){
    createNetwork
    createVolumes
    for new_images in "${NEW_IMAGES[@]}" ; do
        #echo $new_images
        #her goes docker pull cmd
        $DOCKER_CMD pull $new_images
        
    done
}

runContainers(){
    for container_name in "${NEW_IMAGES[@]}" ; do
        folder_name="${container_name//\/}"
        echo "Runnin $container_name ..."
        echo $container_name
        #here goes cmd for Run container
        $DOCKER_CMD run -d --name=$folder_name --network=$BRIDGE_NAME --expose 80 -v "$(pwd)"/$folder_name:/var/$folder_name $container_name touch /var/$folder_name
    done
}

#ask if wnat to pull images
#echo "Do you want to pull images? [y,n]"
read -n1 -p "Do you want to pull images? [y,n]" -i Y CHOICE

if [[ "$CHOICE" = "Y" || "$CHOICE" = "y" ]] ; 
then
    printf "\n\n==== Pulling Images ====\n"
    pullImages
    
else
    printf "\n\n==== Aborting image Pulling ===="
fi

#ask for run containers
read -n1 -p "Do you want to RUN containers? [y,n]" -i Y CHOICE
if [[ "$CHOICE" = "Y" || "$CHOICE" = "y" ]] ; 
then
    printf "\n\n==== Running containers ====\n"
    runContainers
    
else
    printf "\n\n==== Aborting running containers ===="
fi